add_library("sound")

theta = PI/2
shipPositionX = 500
shipPositionY = 500
shipSpeedX = 0
shipSpeedY = 0
shipSpeedTotal = shipSpeedX + shipSpeedY
shooter = False
ranger = 0
shotFromX = 0
shotFromY = 0
shotFromTheta = 0
noAsteroid = True
asteroidX = []
asteroidY = []
asteroidSpeedX = []
asteroidSpeedY = []
xBull = -100
yBull = -100
ALIVE = True
playerScore = 0
osil = 0
asteroidsTotal = 0
timer = 0
leftTurner = False
rightTurner = False
accelerator = False

def setup():
    size(1000,1000)
    generate_starlist()   
    noFill() 
    strokeWeight(1)
    stroke(255,255,255)
    frameRate(60)
    
    
############################DRAWING FUNCTIONS######################################    


def draw():
    """main funcion where 90% of the game is played/called from.  some other events
    do trigger draws but don't make logical sense to put in the draw function.
    
    on the other hand, some of the functions called in this function pertain more to
    game state than actual drawing, only because it makes sense to have these functions
    called in a "main loop" because of the frequency they need to be called"""
    
    global ranger, shooter, shotFromX, shotFromY, shotFromTheta, osil
    global playerScore, asteroidX, asteroidY, xBull, yBull, noAsteroid, ALIVE
    global asteroidsTotal, timer
    ####STAR DRAWING LOOP####
    i = 0
    while i < 200:
        ellipse(xStarList[i],yStarList[i], zStarList[i],zStarList[i])
        i += 1
    #noStroke()

    ####GAME STATE CHECK####     
    if ALIVE == True:
        shipPositioner()
        draw_ship()
        spawnAsteroid()
        asteroidPositioner()
        drawAsteroid()
    fill(0)
    rect(-10,-10, 1020, 175)
    strokeWeight(1)
    noFill()
        
    draw_score()
        
    ####BULLET STATE CHECK####
    if shooter == True:
        #print(ranger)
        if ranger == 0:
            shotFromX = shipPositionX
            shotFromY = shipPositionY
            shotFromTheta = theta
            ranger = 30
            # shootSound = SoundFile(this, "fire.wav")
            # shootSound.rate(.25)
            # shootSound.play()
        xBull, yBull = bulletPosition(shotFromX, shotFromY, shotFromTheta)
        ellipse(xBull, yBull, 5, 5)
        ranger += 10
        
    ####COLLISION STATE CALLS####
   
    collisionDetector()
    
    z = 0    
    while z < asteroidsTotal:
        bulletCollider(z)
        z += 1
    
    fill(0,0,0,90)
    rect(-5,-5,1005,1005)
    
    ####SECRET MODE FOR PRO PLAYER####
    if playerScore > 20:
        r, b, g = chromaticChanger(255, osil)
        osil = osil + 0.04
        stroke(r,b,g)
        
    timer = timer + 1
    
    # if timer % 60 == 0 and (not(timer % 120 == 0)):
    #     lowBeat = SoundFile(this, "beat1.wav")
    #     lowBeat.rate(.25)
    #     lowBeat.play()   
        
    # if timer % 120 == 0:
    #     highBeat = SoundFile(this, "beat2.wav")
    #     highBeat.rate(.25)
    #     highBeat.play()   
    #     timer = 0        
    

    if rightTurner == True:
        rotator(1)
    if leftTurner == True:
        rotator(-1)
    if accelerator == True:
        shipAccelerator(1)
        drawThruster()
    print accelerator
    
def chromaticChanger(b, osil):
    """HIDDEN BONUS FOR VERY GOOD PLAYER"""
    return b*cos(osil)+128,b*cos(osil+PI*4/3)+128, b*cos(osil+PI*2/3)+128    
    
def drawThruster():
    """draws a small ellipse at the rear of the ship when accelerating"""
    ellipse(shipPositionX - 20*cos(theta), shipPositionY - 20*sin(theta), 10, 10)
    # thrusterSound = SoundFile(this, "thrust.wav")
    # thrusterSound.rate(.25)
    # thrusterSound.play()
            
def drawAsteroid():
    """function in charge of drawing the asteroids from their list, the largest portion
    of this code is taken up by the graphical drawing of the asteroid itself, which is then 
    repeated i times"""
    global asteroidX, asteroidY, asteroidSpeedX, asteroidSpeedY, asteroidSize, asteroidsTotal
    i = 0
    while i < asteroidsTotal:
        line( asteroidX[i] -23 * asteroidSize[i], asteroidY[i] +1  * asteroidSize[i],
              asteroidX[i] -21 * asteroidSize[i], asteroidY[i] -13 * asteroidSize[i])
        line( asteroidX[i] -21 * asteroidSize[i], asteroidY[i] -13 * asteroidSize[i],
              asteroidX[i] -2  * asteroidSize[i], asteroidY[i] -24 * asteroidSize[i]) 
        line( asteroidX[i] -2  * asteroidSize[i], asteroidY[i] -24 * asteroidSize[i],
              asteroidX[i] +20 * asteroidSize[i], asteroidY[i] -14 * asteroidSize[i])     
        line( asteroidX[i] +20 * asteroidSize[i], asteroidY[i] -14 * asteroidSize[i],
              asteroidX[i] +18 * asteroidSize[i], asteroidY[i] -7  * asteroidSize[i])         
        line( asteroidX[i] +18 * asteroidSize[i], asteroidY[i] -7  * asteroidSize[i],
              asteroidX[i] +24 * asteroidSize[i], asteroidY[i] +3  * asteroidSize[i])         
        line( asteroidX[i] +24 * asteroidSize[i], asteroidY[i] +3  * asteroidSize[i],
              asteroidX[i] +14 * asteroidSize[i], asteroidY[i] +20 * asteroidSize[i]) 
        line( asteroidX[i] +14 * asteroidSize[i], asteroidY[i] +20 * asteroidSize[i],
              asteroidX[i] -10 * asteroidSize[i], asteroidY[i] +21 * asteroidSize[i]) 
        line( asteroidX[i] -10 * asteroidSize[i], asteroidY[i] +21 * asteroidSize[i],
              asteroidX[i] -23 * asteroidSize[i], asteroidY[i] +1  * asteroidSize[i])    
       # ellipse(asteroidX[i], asteroidY[i], 100, 100) ####this is the hit detection ellipse####
        i+=1
        
def draw_ship():
    """this function draws the ship"""
    global theta, shipPositionX, shipPositionY
    
    # line(shipPositionX,
    #      shipPositionY, 
         
    #      20*cos(theta) + shipPositionX,
    #      20*sin(theta) + shipPositionY)
    
    #fin right side
    line(20*cos(theta + PI/1.2) + shipPositionX, 
          20*sin(theta + PI/1.2) + shipPositionY,
          shipPositionX, shipPositionY)
    
    #fin left side
    line(shipPositionX, shipPositionY,
          20*cos(theta - PI/1.2) + shipPositionX,
          20*sin(theta - PI/1.2) + shipPositionY)

    #wing right side   
    line(20*cos(theta) + shipPositionX, 
         20*sin(theta) + shipPositionY,
         
          20*cos(theta + PI/1.2) + shipPositionX, 
          20*sin(theta + PI/1.2) + shipPositionY)
    

    #wing left side  
    line(20*cos(theta) + shipPositionX, 
         20*sin(theta) + shipPositionY,
         
          20*cos(theta - PI/1.2) + shipPositionX,
          20*sin(theta - PI/1.2) + shipPositionY)  

def draw_score():
    """function in charge of taking the player score, and passing the value along to the
    draw digital function"""
    global playerScore
    textSize(30)
    draw_digital(0, 600, 50)
    draw_digital(0, 550, 50)
    draw_digital(playerScore % 10, 500, 50)
    draw_digital(playerScore/10, 450, 50)
    draw_digital(playerScore/100, 400, 50)
    #text("SCORE:", 500, 50)
    #text(100*playerScore, 500, 100)
            
def draw_digital(modulo, x, y):
    """function in charge of drawing the "digital" numbers, every particle is controlled
    through a boolean value"""
    a = False
    b = False
    c = False
    d = False
    e = False
    f = False
    g = False
    if modulo == 1:
        a = True
        b = True
    elif modulo == 2:
        c = True
        a = True
        g = True
        e = True
        d = True
    elif modulo == 3:
        c = True
        a = True
        g = True
        b = True
        d = True
    elif modulo == 4:
        f = True
        g = True
        a = True
        b = True
    elif modulo == 5:
        c = True
        f = True
        g = True
        b = True
        d = True
    elif modulo == 6:
        c = True
        f = True
        b = True
        g = True
        e = True
        d = True
    elif modulo == 7:
        c = True
        a = True
        b = True
    elif modulo == 8:
        a = True
        b = True
        c = True
        d = True
        e = True
        f = True
        g = True
    elif modulo == 9:
        a = True
        b = True
        c = True
        d = True
        f = True
        g = True
    elif modulo == 0:
        a = True
        b = True
        c = True
        d = True
        e = True
        f = True
    
    if c == True:
        rect(5 + x, 0 + y, 30, 5)
    if f == True:
        rect(0 + x, 5 + y, 5, 30)
    if e == True:
        rect(0 + x, 40 + y, 5, 30)
    if g == True:
        rect(5 + x, 35 + y, 30, 5)
    if d == True:
        rect(5 + x, 70 + y, 30, 5)
    if a == True:
        rect(35 + x, 5 + y, 5, 30)
    if b == True:
        rect(35 + x, 40 + y, 5, 30)
        
##############################SPAWNING FUNCTIONS#########################################

def spawnAsteroid():
    """spawns a new asteroid when there are no asteroids"""
    global asteroidX, asteroidY, asteroidSpeedX, asteroidSpeedY, noAsteroid, asteroidSize, asteroidsTotal
    if noAsteroid == True:
        q = 0
        asteroidX = []
        asteroidY = []
        asteroidSpeedX = []
        asteroidSpeedY = []
        asteroidSize = []
        while q < 5:
            asteroidX.append(random(1000))
            asteroidY.append(random(1000))  
            asteroidSpeedX.append(random(-2,2))
            asteroidSpeedY.append(random(-2,2))
            asteroidSize.append(2)
            asteroidsTotal = asteroidsTotal + 1
            q += 1
        noAsteroid = False    
        #print(asteroidX)
                        
def generate_starlist():
    """Generates a list of X and Y coordinates, and sizes for the starfield, the reason it needs to be stored is so it can
    be redrawn the same way after a screen need to be wiped after a 'hit'"""
    global xStarList, yStarList, zStarList
    i = 0
    xStarList = []
    yStarList = []
    zStarList = []
    while i < 200:
        x = random(1000)
        y = random(1000)
        z = random(1,5)
        xStarList.append(x)
        yStarList.append(y)
        zStarList.append(z)
        i += 1      
        
###########################POSITION/SPEED/COLLISION FUNCTIONS###########################################          
                
def rotator(torque):
    """this function is in charge of direction that the ship is turned"""
    global theta
    if torque == 1:
        theta = theta + PI / 30
    elif torque == -1:
        theta = theta - PI / 30
    else:
        return
    
def shipAccelerator(accel):  
    """
    function in charge calculating the change in the speed of the ship if the accelerator is pushed
    also in charge of capping the speed of the ship in either direction, as well as the breaks essentially a
    decelerrator, which is why it's included here"""
    
    global theta, shipSpeedX, shipSpeedY, shipSpeedTotal
    if accel == 1 :
        ####SHIP SPEED CALCULATOR####
        shipSpeedX = shipSpeedX + 0.5*cos(theta)
        shipSpeedY = shipSpeedY + 0.5*sin(theta)
        ####MAX SPEED CAPS####
        if shipSpeedX > 15:
            shipSpeedX = 15
        if shipSpeedY > 15:
            shipSpeedY = 15
        if shipSpeedX < -15:
            shipSpeedX = -15
        if shipSpeedY < -15:
            shipSpeedY = -15
    elif accel == -1:
        ####BREAK FUNCTION####
        if shipSpeedX > 0:
            shipSpeedX = shipSpeedX - 0.1
        if shipSpeedY > 0:   
            shipSpeedY = shipSpeedY - 0.1
        if shipSpeedX < 0:
            shipSpeedX = shipSpeedX + 0.1
        if shipSpeedY < 0:   
            shipSpeedY = shipSpeedY + 0.1
        if shipSpeedX < .2 and shipSpeedX > -.2 and shipSpeedY < .2 and shipSpeedY > -.2:
            shipSpeedX = 0
            shipSpeedY = 0
    else:
        return
    
def bulletPosition(shotFromX, shotFromY, shotFromTheta):
    "this function only returns the position of the bullets"""
    return shotFromX + ranger*cos(shotFromTheta), shotFromY + ranger*sin(shotFromTheta)
    
def hyperSpacer():
    """classic hyperspacer from original asteroids"""
    global shipPositionX, shipPositionY
    shipPositionX = random(1000)
    shipPositionY = random(1000)
        
def shipPositioner():
    """this ads the acceleration value on to the ships position"""
    global shipAccelX, shipAccelY, shipPositionX, shipPositionY
    shipPositionX = shipPositionX + shipSpeedX
    shipPositionY = shipPositionY + shipSpeedY
    
    """
    this section is incharge of wrapping the ship if it hits the edge of the screen
    """
    if  shipPositionX > 1010:
        shipPositionX = -10
    if  shipPositionY > 1010:
        shipPositionY = -10
    if  shipPositionX < -11:
        shipPositionX = 1009
    if  shipPositionY < -11:
        shipPositionY = 1009
            
def asteroidPositioner():
    """keeps track of the asteroid"""
    global asteroidX, asteroidY, asteroidSpeedX, asteroidSpeedY, asteroidsTotal
    i = 0
    while i < asteroidsTotal:
        asteroidX[i] = asteroidX[i] + asteroidSpeedX[i]
        asteroidY[i] = asteroidY[i] + asteroidSpeedY[i]
        """this section wraps the asteroid"""
        
        #####MAX EDGE LOOP CONDITIONS####
        if  asteroidX[i] > 1050:
            asteroidX[i]  = -50
        if  asteroidY[i] > 1050:
            asteroidY[i] = -50
            
        ####MIN EDGE LOOP CONDITIONDS#### + NOT FOR "HIDDEN AREA"
        if  asteroidX[i] < -51 and asteroidX[i] != -2000:
            asteroidX[i]  = 1049
        if  asteroidY[i] < -51:
            asteroidY[i] = 1049
        i += 1    

def bulletCollider(z):
    """function in charge of bullet collision detection, as well as spawning new asteroids and appending
    them to the asteroid list when a hit is confirmed"""
    global ranger, shooter, shotFromX, shotFromY, shotFromTheta, osil
    global playerScore, asteroidX, asteroidY, xBull, yBull, noAsteroid, ALIVE
    global asteroidsTotal, timer
    
    ####IF HIT NON-ZERO ASTEROID####
    if dist(xBull, yBull, asteroidX[z], asteroidY[z]) < (asteroidSize[z] * 25) and asteroidSize[z] > .5:
        asteroidSpeedX[z] = random(-2, 2)
        asteroidSpeedY[z] = random(-2, 2)
        asteroidSize[z] = asteroidSize[z] - .50
        asteroidsTotal = asteroidsTotal + 1
        asteroidX.append(asteroidX[z])
        asteroidY.append(asteroidY[z])
        asteroidSpeedX.append(random(-2, 2))
        asteroidSpeedY.append(random(-2, 2))
        asteroidSize.append(asteroidSize[z])
        playerScore = playerScore + 1
        # boomSoundLarge = SoundFile(this, "bangLarge.wav")
        # boomSoundLarge.rate(.25)
        # boomSoundLarge.play()
        ranger = 0
        xBull = -500
        yBull = -500
        shooter = False
        
    ####IF HIT ZERO SIZE ASTEROID####
    elif dist(xBull, yBull, asteroidX[z], asteroidY[z]) < (asteroidSize[z] * 25):
        asteroidSpeedX[z] = 0
        asteroidSpeedY[z] = 0
        asteroidX[z] = -2000
        asteroidY[z] = -2000
        playerScore = playerScore + 1
        # boomSoundLarge = SoundFile(this, "bangLarge.wav")
        # boomSoundLarge.rate(.25)
        # boomSoundLarge.play()
        ranger = 0
        xBull = -500
        yBull = -500
        shooter = False
        #z += 1

    ####MAX RANGE CONDITION FOR BULLET####        
    if ranger > 300:
        shooter = False
        ranger = 0
        xBull = - 500
        yBull = - 500

                                    
def collisionDetector():
    """detects the collision between the ship and the asteroid"""
    global ALIVE, asteroidX, asteroidY, shipPositionX, shipPositionY, theta, asteroidSize, asteroidsTotal
    i = 0
    while i < asteroidsTotal:
        
        ####GENERAL CONDITION CASE####
        if dist(shipPositionX, shipPositionY, asteroidX[i], asteroidY[i]) < asteroidSize[i] * 25:
            ALIVE = False
            
        ####WING COLLISION CASES####
        if dist(20*cos(theta + PI/1.2) + shipPositionX, 20*sin(theta + PI/1.2) + shipPositionY,
                     asteroidX[i], asteroidY[i]) < asteroidSize[i] * 25:
            ALIVE = False
        if dist(20*cos(theta - PI/1.2) + shipPositionX, 20*sin(theta - PI/1.2) + shipPositionY,
                     asteroidX[i], asteroidY[i]) < asteroidSize[i] * 25:
            ALIVE = False
            
        ####TIP COLLISION CASE#####
        if dist(20*cos(theta) + shipPositionX, 20*sin(theta) + shipPositionY, asteroidX[i], asteroidY[i]) < asteroidSize[i] * 25:
            ALIVE = False
        i +=1
        
####################################CONTROLS#####################################################  

def keyReleased():
    global leftTurner, rightTurner, accelerator
    if key == "d":
        rightTurner = False
    if key == "a":
        leftTurner = False
    if key == "w":
        accelerator = False
        
                
def keyPressed():
    global shooter, ranger, ALIVE, shipPositionX, shipPositionY, noAsteroid, shipSpeedX, shipSpeedY
    global playerScore, theta, asteroidX, asteroidY, asteroidsTotal, leftTurner, rightTurner, accelerator
    
    ####KEYPRESS FUNCTIONS####
    
    if key == "d":
        rightTurner = True
    elif key == "a":
        leftTurner = True
    elif key == "w" and ALIVE == True:
        accelerator = True
    elif key == "r":
        shipAccelerator(-1)
    elif key == " ":
        hyperSpacer()
    elif key == "e" and ALIVE == True and shooter != True:
        ranger = 0
        shooter = True
    elif key == "1" and ALIVE == False:
        ALIVE = True
        shipPositionX = 500
        shipPositionY = 500
        asteroidX = []
        asteroidY = []
        asteroidsTotal = 0
        noAsteroid = True
        shipSpeedX = 0
        shipSpeedY = 0
        playerScore = 0
        theta = PI/2
        
    ####CAPITAL IF CASE####
    
    elif key == "D":
        rotator(1)
    elif key == "A":
        rotator(-1)
    elif key == "W" and ALIVE == True:
        shipAccelerator(1)
        drawThruster()
    elif key == "R":
        shipAccelerator(-1)
    elif key == "E" and ALIVE == True:
        ranger = 0
        shooter = True

    

        